import System.Environment (getArgs)

interactWith function input_file output_file = do
    input <- readFile input_file
    writeFile output_file (function input)


myFunction = unwords . words

main = mainWith myFunction
    where mainWith function = do
          args <- getArgs
          case args of
              [input, output] -> interactWith function input output
              _ -> putStrLn "Error: Two args required"


