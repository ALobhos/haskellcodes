data BookInfo = Book Int String [String]
                deriving (Show)

data MagazineInfo = Magazine Int String [String]
                    deriving (Show)

libro = Book 1 "Dracula" ["Brams Stocker"]
