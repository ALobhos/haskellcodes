myPalindrome :: [a] -> [a]
myPalindrome (x:xs) = [x] ++ myPalindrome xs ++ [x] 
myPalindrome [] = []
