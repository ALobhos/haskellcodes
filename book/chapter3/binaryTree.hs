data BinaryTree a = Node a (Maybe (BinaryTree a)) (Maybe (BinaryTree a))
                    deriving (Show)


myTree = Node "hello" Nothing (Just(Node "hey" Nothing Nothing))
