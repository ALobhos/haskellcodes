temperature :: Integer ->  Double
temperature n
  | n > 0 = (n/4 + 40)
  | otherwise = 1.0

main = do
    print "Ingrese cantidad de sonidos emitidos"
    sounds <- getLine
    print "La temperatura es"
    print(temperature (read sounds::Integer))

