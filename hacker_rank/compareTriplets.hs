convertToIntegers xs = map read xs :: [Integer]

compareTriplets xs ys = zipWith compare xs ys

getScores [] = [0,0]
getScores (x:xs)
    | x == EQ = zipWith (+) [0,0] (getScores xs)
    | x == LT = zipWith (+) [0,1] (getScores xs)
    | x == GT = zipWith (+) [1,0] (getScores xs)

main :: IO ()
main = do
    alice <- getLine
    bob <- getLine 
    let bobTriplet = convertToIntegers $ words bob
    let aliceTriplet = convertToIntegers $ words alice
    putStr(unwords 
         $ map show 
         $ getScores 
         $ compareTriplets aliceTriplet bobTriplet)
