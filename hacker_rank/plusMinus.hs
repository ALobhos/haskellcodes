import Data.Fixed

truncate' xs = map fromRational xs :: [Micro]

convertToList xs = map read $ words xs :: [Int]

getProportion xs = map  ((/ l) .  fromRational)  xs  
                   where
                       l = fromRational (sum xs) :: Rational

checkValues (x:xs)
    | x == 0 = zipWith (+) [0,0,1] (checkValues xs)
    | x >  0 = zipWith (+) [1,0,0] (checkValues xs)
    | x <  0 = zipWith (+) [0,1,0] (checkValues xs)
checkValues [] = [0,0,0]


main = do
    _ <- getLine
    numbers <- getLine
    putStr(unlines
          $ map show
          $ truncate'
          $ getProportion 
          $ checkValues 
          $ convertToList numbers)

